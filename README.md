This extension was built by distriqt // 

# Bolts

Bolts is package ANE that includes the Bolts framework for use in the Facebook and Parse ANE's.

It contains both the Android and iOS version of the Bolts Framework.



## Documentation

See the appropriate ANE documentation for usage of this ANE:

http://airnativeextensions.com/extension/com.distriqt.FacebookAPI
http://airnativeextensions.com/extension/com.distriqt.Parse



## License

You can purchase a license for using this extension:

http://airnativeextensions.com

distriqt retains all copyright.
