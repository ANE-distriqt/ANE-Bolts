

###### 2016.02.05

Updated SDK - iOS: v1.5.1 Android: v1.4.0


###### 2015.08.25

Android: Updated Bolts lib to version 1.2.1


###### 2015.06.18

Release of the ANE (Android v1.2 iOS v1.1.5)
